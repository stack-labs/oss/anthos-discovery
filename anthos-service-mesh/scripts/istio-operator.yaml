apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
metadata:
  annotations:
    install.istio.io/ignoreReconcile: "true"
  name: installed-state-asm-193-2
  namespace: istio-system
spec:
  components:
    base:
      enabled: true
    cni:
      enabled: false
    egressGateways:
      - enabled: false
        k8s:
          env:
            - name: ISTIO_META_ROUTER_MODE
              value: standard
          hpaSpec:
            maxReplicas: 5
            metrics:
              - resource:
                  name: cpu
                  targetAverageUtilization: 80
                type: Resource
            minReplicas: 2
            scaleTargetRef:
              apiVersion: apps/v1
              kind: Deployment
              name: istio-egressgateway
          resources:
            limits:
              cpu: 2000m
              memory: 1024Mi
            requests:
              cpu: 100m
              memory: 128Mi
          service:
            ports:
              - name: http2
                port: 80
                protocol: TCP
                targetPort: 8080
              - name: https
                port: 443
                protocol: TCP
                targetPort: 8443
              - name: tls
                port: 15443
                protocol: TCP
                targetPort: 15443
          strategy:
            rollingUpdate:
              maxSurge: 100%
              maxUnavailable: 25%
        name: istio-egressgateway
    ingressGateways:
      - enabled: true
        k8s:
          env:
            - name: ISTIO_META_ROUTER_MODE
              value: standard
          hpaSpec:
            maxReplicas: 5
            metrics:
              - resource:
                  name: cpu
                  targetAverageUtilization: 80
                type: Resource
            minReplicas: 2
            scaleTargetRef:
              apiVersion: apps/v1
              kind: Deployment
              name: istio-ingressgateway
          resources:
            limits:
              cpu: 2000m
              memory: 1024Mi
            requests:
              cpu: 100m
              memory: 128Mi
          service:
            ports:
              - name: status-port
                port: 15021
                protocol: TCP
                targetPort: 15021
              - name: http2
                port: 80
                protocol: TCP
                targetPort: 8080
              - name: https
                port: 443
                protocol: TCP
                targetPort: 8443
              - name: tcp-istiod
                port: 15012
                protocol: TCP
                targetPort: 15012
              - name: tls
                port: 15443
                protocol: TCP
                targetPort: 15443
          strategy:
            rollingUpdate:
              maxSurge: 100%
              maxUnavailable: 25%
        name: istio-ingressgateway
    istiodRemote:
      enabled: false
    pilot:
      enabled: true
      k8s:
        env:
          - name: GKE_CLUSTER_URL
            value: https://container.googleapis.com/v1/projects/kevin-anthos-asm/locations/europe-north1-a/clusters/anthos-asm-demo
          - name: GCP_METADATA
            value: kevin-anthos-asm|62405001080|anthos-asm-demo|europe-north1-a
          - name: SPIFFE_BUNDLE_ENDPOINTS
            value: kevin-anthos-asm.svc.id.goog|https://storage.googleapis.com/mesh-ca-resources/spiffe_bundle.json
          - name: ENABLE_STACKDRIVER_MONITORING
            value: "true"
          - name: TOKEN_AUDIENCES
            value: istio-ca,kevin-anthos-asm.svc.id.goog
          - name: PILOT_ENABLE_WORKLOAD_ENTRY_AUTOREGISTRATION
            value: "true"
        hpaSpec:
          minReplicas: 2
        replicaCount: 2
        resources:
          requests:
            cpu: 500m
  hub: gcr.io/gke-release/asm
  meshConfig:
    enableTracing: true
    values:
      global:
        proxy:
          tracer: stackdriver
    defaultConfig:
      tracing:
        sampling: 100
      proxyMetadata:
        CA_PROVIDER: GoogleCA
        GCE_METADATA_HOST: metadata.google.internal
        GCP_METADATA: kevin-anthos-asm|62405001080|anthos-asm-demo|europe-north1-a
        GKE_CLUSTER_URL: https://container.googleapis.com/v1/projects/kevin-anthos-asm/locations/europe-north1-a/clusters/anthos-asm-demo
        PLUGINS: GoogleTokenExchange
        USE_TOKEN_FOR_CSR: "true"
    enablePrometheusMerge: true
    localityLbSetting:
      enabled: false
    protocolDetectionTimeout: 0s
    trustDomain: kevin-anthos-asm.svc.id.goog
    trustDomainAliases:
      - kevin-anthos-asm.svc.id.goog
  profile: empty
  revision: asm-193-2
  tag: 1.9.3-asm.2
  values:
    base:
      enableCRDTemplates: false
      validationURL: ""
    gateways:
      istio-egressgateway:
        autoscaleEnabled: true
        env: {}
        name: istio-egressgateway
        secretVolumes:
          - mountPath: /etc/istio/egressgateway-certs
            name: egressgateway-certs
            secretName: istio-egressgateway-certs
          - mountPath: /etc/istio/egressgateway-ca-certs
            name: egressgateway-ca-certs
            secretName: istio-egressgateway-ca-certs
        type: ClusterIP
        zvpn: {}
      istio-ingressgateway:
        autoscaleEnabled: true
        env: {}
        name: istio-ingressgateway
        secretVolumes:
          - mountPath: /etc/istio/ingressgateway-certs
            name: ingressgateway-certs
            secretName: istio-ingressgateway-certs
          - mountPath: /etc/istio/ingressgateway-ca-certs
            name: ingressgateway-ca-certs
            secretName: istio-ingressgateway-ca-certs
        type: LoadBalancer
        zvpn: {}
    global:
      arch:
        amd64: 2
        ppc64le: 2
        s390x: 2
      caAddress: meshca.googleapis.com:443
      configValidation: true
      defaultNodeSelector: {}
      defaultPodDisruptionBudget:
        enabled: true
      defaultResources:
        requests:
          cpu: 10m
      imagePullPolicy: ""
      imagePullSecrets: []
      istioNamespace: istio-system
      istiod:
        enableAnalysis: false
      jwtPolicy: third-party-jwt
      logAsJson: false
      logging:
        level: default:info
      meshID: proj-62405001080
      meshNetworks: {}
      mountMtlsCerts: false
      multiCluster:
        clusterName: cn-kevin-anthos-asm-europe-north1-a-anthos-asm-demo
        enabled: false
      network: kevin-anthos-asm-default
      omitSidecarInjectorConfigMap: false
      oneNamespace: false
      operatorManageWebhooks: false
      pilotCertProvider: kubernetes
      priorityClassName: ""
      proxy:
        autoInject: enabled
        clusterDomain: cluster.local
        componentLogLevel: misc:error
        enableCoreDump: false
        excludeIPRanges: ""
        excludeInboundPorts: ""
        excludeOutboundPorts: ""
        image: proxyv2
        includeIPRanges: '*'
        logLevel: warning
        privileged: false
        readinessFailureThreshold: 30
        readinessInitialDelaySeconds: 1
        readinessPeriodSeconds: 2
        resources:
          limits:
            cpu: 2000m
            memory: 1024Mi
          requests:
            cpu: 100m
            memory: 128Mi
        statusPort: 15020
        tracer: zipkin
      proxy_init:
        image: proxyv2
        resources:
          limits:
            cpu: 2000m
            memory: 1024Mi
          requests:
            cpu: 10m
            memory: 10Mi
      sds:
        token:
          aud: kevin-anthos-asm.svc.id.goog
      sts:
        servicePort: 15463
      tracer:
        datadog: {}
        lightstep: {}
        stackdriver: {}
        zipkin: {}
      useMCP: false
    istiodRemote:
      injectionURL: ""
    pilot:
      autoscaleEnabled: true
      autoscaleMax: 5
      autoscaleMin: 1
      configMap: true
      cpu:
        targetAverageUtilization: 80
      deploymentLabels: null
      enableProtocolSniffingForInbound: false
      enableProtocolSniffingForOutbound: false
      env: {}
      image: pilot
      keepaliveMaxServerConnectionAge: 30m
      nodeSelector: {}
      replicaCount: 1
      traceSampling: 1
    sidecarInjectorWebhook:
      rewriteAppHTTPProbe: true
    telemetry:
      enabled: true
      v2:
        enabled: true
        metadataExchange:
          wasmEnabled: false
        prometheus:
          enabled: false
          wasmEnabled: false
        stackdriver:
          configOverride: {}
          enabled: true
          inboundAccessLogging: FULL
          logging: false
          monitoring: false
          outboundAccessLogging: ERRORS_ONLY
          topology: true
